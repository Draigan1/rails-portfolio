class Skill < ActiveRecord::Base
  has_many :projects_skills
  has_many :projects, through: :projects_skills
end
