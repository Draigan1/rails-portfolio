class Project < ActiveRecord::Base
  has_many :projects_skills
  has_many :skills, through: :projects_skills
end
