class HomepagesController < ApplicationController
  def index
    @projects = Project.all.includes(:skills).to_a
    @skills = Skill.all.includes(:projects).to_a
  end
end
