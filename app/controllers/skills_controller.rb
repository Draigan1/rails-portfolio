class SkillsController < ApplicationController
  def index
    @skills = Skill.all.includes(:projects).to_a
  end

  def show
    @skill = Skill.find(params[:id])
  end
end
