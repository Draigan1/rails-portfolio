class ProjectsController < ApplicationController
  def index
    @slug_filter = params[:using]
    @projects = if @slug_filter.present?
                  Project.all.where(skills: { slug: @slug_filter }).includes(:skills).to_a
                else
                  Project.all.includes(:skills).to_a
                end
    @skills = Skill.all
  end

  def show
    @project = Project.find(params[:id])
  end
end
