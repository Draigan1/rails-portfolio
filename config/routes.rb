Rails.application.routes.draw do # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  # Defines the root path route ("/")
  root 'homepages#index'
  resources :skills, only: %i[index show]
  resources :projects, only: %i[index show]
  resources :homepages, only: [:index]
  resources :resumes, only: [:index]
  resources :contacts, only: [:index]
end
