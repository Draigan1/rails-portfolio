class CreateProjects < ActiveRecord::Migration[7.0]
  def change
    create_table :projects do |t|
      t.timestamps
      t.string :title
      t.string :image
      t.string :slug
      t.string :github_url
      t.string :alt
      t.boolean :custom_content
      t.boolean :featured
      t.text :description
      t.text :long_description
    end
  end
end
