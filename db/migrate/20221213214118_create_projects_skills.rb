class CreateProjectsSkills < ActiveRecord::Migration[7.0]
  def change
    create_table :projects_skills do |t|
      t.timestamps
      t.belongs_to :skill
      t.belongs_to :project
    end
  end
end
