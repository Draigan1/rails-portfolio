class CreateSkills < ActiveRecord::Migration[7.0]
  def change
    create_table :skills do |t|
      t.timestamps
      t.string :name
      t.string :slug
      t.string :image
      t.string :alt
      t.boolean :featured
      t.text :description
      t.text :long_description
    end
  end
end
