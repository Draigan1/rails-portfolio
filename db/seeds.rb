# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
# projects = YAML.load(File.open('../portfolio/data/projects.yml'))
# projects.each do |p|
#   project = Project.new
#   project.assign_attributes(p)
#   project.save!
# end

# skills = YAML.load(File.open('../portfolio/data/skills.yml'))
# skills.each do |s|
#   skill = Skill.new
#   skill.assign_attributes(s)
#   skill.save!
# end

projects_skills = YAML.load(File.open('../portfolio/data/projects_skills.yml'))
projects_skills.each do |ps|
  project_skill = ProjectsSkill.new
  project_skill.project = Project.find(ps['project_id'])
  project_skill.skill = Skill.find(ps['skill_id'])
  project_skill.save!
end
